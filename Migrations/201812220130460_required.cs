namespace ProductCatalog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class required : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Products", "name", c => c.String(nullable: false));
            AlterColumn("dbo.Products", "priority", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "priority", c => c.String());
            AlterColumn("dbo.Products", "name", c => c.String());
        }
    }
}
